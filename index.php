<?php 

include_once 'Information.php';

$information = new Information;

echo 'Time Limit = ' . ini_get('max_execution_time') . '<br/>';

if(isset($_POST['submit']))
{
	$information -> insertToDatabase($_POST['totalNumber']);
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Big Data</title>
</head>
<body>

	<h2>Now Total Row: 
		<?php 
			echo $information -> totalRow();
		?>
	</h2>

	<form action="" method="post">
		<label for="totalNumber">Insert: </label>
		<input type="number" name="totalNumber" id="totalNumber"/>
		<input type="submit" name="submit" value="Insert"/>
	</form>
</body>
</html>
