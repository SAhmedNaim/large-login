<?php

ini_set('max_execution_time', 1800);
// set_time_limit(1);
// echo 'Time Limit = ' . ini_get('max_execution_time') . '<br/>';

include_once 'Database.php';

class Information
{
	private $db;

	public function __construct() 
	{
		$this -> db = new Database();
	}
	
	public function generateRandomString($length) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) 
	    {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function generateRandomName($length) 
	{
	    $characters = 'abcdefghijklmnopqrstuvwxyz';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) 
	    {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function generateRandomEmail($length) 
	{
	    $characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) 
	    {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	// function test()
	// {
	// 	$total = rand(20, 30); // 25
	// 	$first = rand(5, 10); // 8
	// 	$second = $total - $first; // 1 + 16
	// 	$third = rand(5, $second - 4); // 1 + 8
	// 	$forth = $total - ($first + $third + 2); // 8 + 8 + 2 (7)
	// 	$fifth = rand($forth); // 25 == 8 + 1 + 8 + 1 + 7
	// }

	public function generateUsername()
	{
		$totalDigit = rand(20, 30);

		$firstNameDigit = rand(5, 10);
		$firstName = $this -> generateRandomName($firstNameDigit);

		$middleNameDigit = $totalDigit - $firstNameDigit - 4;
		$middleName = $this -> generateRandomName(rand(5, $middleNameDigit));

		$lastNameDigit = $totalDigit - ($firstNameDigit + $middleNameDigit);
		$lastName = $this -> generateRandomName(rand(3, $lastNameDigit));

		$username = ucfirst($firstName) . ' ' . ucfirst($middleName) . ' ' . ucfirst($lastName);

		$digit = $totalDigit . ' = ' . $firstNameDigit . ' + ' . $middleNameDigit . ' + ' . $lastNameDigit;
		return $username;
	}

	// function generateUsername_old()
	// {
	// 	$total = rand(20, 30); // 25
		
	// 	$firstName = rand(5, 10); // 7
	// 	$one = generateRandomString($firstName) . ' '; // 7 + 1
		
	// 	$temp1 = $total - $firstName; //  25 - 7 = 18
		
	// 	$middleName = rand(5, $temp1 - 4); // 11
	// 	$two = generateRandomString($middleName);
	// 	$temp2 = $total - ($firstName + $middleName + 2); // 25 - (7 + 11 + 2) == 5
	// 	$three = generateRandomString($temp2); // 5

	// 	echo $one. '<br/>' . $two. '<br/>' . $three;
	// }

	public function insertBigData()
	{
		$nid = rand(100000, 999999).rand(100000, 999999).rand(100000, 999999);

		$username = $this -> generateUsername();

		$mobile = '01' . rand(1000, 9999) . rand(10000, 99999);

		$email = $this -> generateRandomEmail(15) . '@gmail.com';

		$password = $this -> generateRandomString(8);

		$this -> insertData($nid, $username, $mobile, $email, $password);
	}

	public function insertData($nid, $username, $mobile, $email, $password)
	{
		$sql = "INSERT INTO data_table(nid, username, mobile, email, password) VALUES(:nid, :username, :mobile, :email, :password);";
	    $query = $this->db->pdo->prepare($sql);
	    $query->bindValue(':nid', $nid);
		$query->bindValue(':username', $username);
		$query->bindValue(':mobile', $mobile);
		$query->bindValue(':email', $email);
		$query->bindValue(':password', $password);
		$result = $query->execute();
	}

	public function insertToDatabase($limit)
	{
		for($i = 1; $i <= $limit; $i++)
		{
			// echo '<h2>'.$i.'</h2><hr/>';

			$this -> insertBigData();
		}

		echo 'Total: '.($i-1).' Data Inserted Successfully!!!';
	}

	// return total row
	public function totalRow()
	{
		$sql = "SELECT count(*) FROM data_table;";
		$query = $this->db->pdo->prepare($sql);
		$result = $query->execute();
		$rows = $query -> fetchColumn();
		return $rows;
	}

}